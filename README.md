# mac-cli
#### A command line MAC address conversion utility, with built-in OUI lookup.
Tested on Python 2.7 and 3.4+

**Usage:**  
mac-cli MAC [-h] [-c] [-n] [-d] [-e] [-s] [-f FILE] [-v] [-u | -l]

**Output format:**
```
prompt$ python3 mac-cli.py c8:69:cd:b1:99:ce -cu
C869.CDB1.99CE
Apple
```

TODO:   - 
- Read from NMAP OUI list
- Use extra parameter in slice notation to add colon, dash, etc. in one statement
- Fix copyright symbol
- ~~Global MAC validity function for file lookup~~
- ~~Use True and False instead of 1 and 0.~~
- ~~Add function that checks all convertedX strings and returns true or false~~
- ~~Use "if not myString" to check if converted string is not null~~
- ~~Fix comment placement~~
- ~~Make -o mutually exclusive from -f~~
- ~~Make initial MAC parameter unneeded if -f is used~~
- ~~Pull full OUI string from oui_db.txt~~
- ~~Make -o default and add -s to skip OUI lookup~~
