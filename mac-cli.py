#!/usr/bin/python3
# -*- coding: latin-1 -*-
'''
Created on Sep 4, 2015
@author: Malcolm Bellino
'''
import argparse
import os.path

# Allowed alphanumeric characters in a MAC address
CHARACTERS = 'ABCDEFabcdef0123456789'
# Allowed symbols in MAC address
SYMBOLS = '.:- '

# Checks if MAC is valid and returns empty xxxxxxxxxxxx
def validMAC(mac):
    converted = ""
    for char in mac:
        if char in CHARACTERS:
            converted += char
        elif char not in SYMBOLS:
            print("ERROR: Invalid character in MAC address")
            return False
    if len(converted) < 12: # if converted string is less than 12, error out
        print('ERROR: Too few characters in MAC address.')
        return False
    if len(converted) > 12: # if converted string is longer than 12, error out
        print('ERROR: Too many characters in MAC address.')
        return False
    else:
        return converted

# Checks if MAC is valid and returns empty xxxxxxxxxxxx without printing errors
def validFileMAC(mac):
    converted = ""
    for char in mac:
        if char in CHARACTERS:
            converted += char
        elif char not in SYMBOLS:
            return False
    if len(converted) < 12: # if converted string is less than 12, error out
        return False
    if len(converted) > 12: # if converted string is longer than 12, error out
        return False
    else:
        return converted

# Converts input MAC to Cisco format
def convertCisco(mac):
    convertedList = list(mac) # convert string to list
    convertedList.insert(4, ".")
    convertedList.insert(9, ".")
    mac = ''.join(convertedList)

    return mac

# Converts input MAC to colon format
def convertColon(mac):
    converted = validMAC(mac)
    convertedList = list(mac)
    convertedList.insert(2, ":")
    convertedList.insert(5, ":")
    convertedList.insert(8, ":")
    convertedList.insert(11, ":")
    convertedList.insert(14, ":")
    mac = ''.join(convertedList)

    return mac

# Converts input MAC to dash format
def convertDash(mac):
    convertedList = list(mac)
    convertedList.insert(2, "-")
    convertedList.insert(5, "-")
    convertedList.insert(8, "-")
    convertedList.insert(11, "-")
    convertedList.insert(14, "-")
    mac = ''.join(convertedList)

    return mac

# Looks up OUI information
def ouiLookup(mac):
    result=""
    keyToFind = validMAC(mac)[0:6]

    # Find script working directory
    scriptDir = os.path.dirname(__file__)
    #ouiPath = "oui_db.txt"
    ouiPath = "nmap-mac-prefixes"
    filePath = os.path.join(scriptDir, ouiPath)

    dictionary = {}
    try:
        with open(filePath) as file:
            for line in file:
                mac_key = line.split(" ")[0]
                mac_value = line.split(" ")[1]
                dictionary[mac_key]=mac_value.rstrip()
                #if(mac_key=="")
    except:
        print("Couldn't open file.")
    try:
        result = dictionary[keyToFind.upper()]
    except:
        result = "OUI not found."
    return result

# Prints OUIs from file MAC address list
def fileLookup(path):
    macs = []
    if(os.path.exists(path)):
        with open(path) as file:
            for line in file:
                if validFileMAC(line.rstrip()):
                    macs.append(line.rstrip())
                else: 
                    macs.append("ERROR: MAC address invalid.")
        return macs
    else:
        macs.insert(0,"false")
        return macs

# Convert MAC arguments to various formats and do OUI lookup
def convertMAC(arguments):
    formats = {}
    # positional MAC arguments exists
    if(arguments.MAC):
        if arguments.colon:
            formats["colon"] = convertColon(arguments.MAC)
        if arguments.dash:
            formats["dash"] = convertDash(arguments.MAC)
        if arguments.empty:
            formats["empty"] = validMAC(arguments.MAC)
        if arguments.cisco:
            formats["cisco"] = convertCisco(arguments.MAC)
        # If no options specified, use default Cisco format
        elif not formats:
            formats["cisco"] = convertCisco(arguments.MAC)
        # -s --skip exists
        if(arguments.skip):
            pass
        else: # Else perform OUI lookup
            formats["oui"] = ouiLookup(arguments.MAC)
        return formats

# Create argument parser object
parser = argparse.ArgumentParser(description="This program converts MAC \
                                addresses to various formats.",
                                prog='mac-cli',
                                epilog='© Malcolm Bellino 2015-2016')
# Add required positional argument
parser.add_argument("MAC",
                    nargs="?",
                    help="The MAC address to be converted. Spaces are \
                    allowed if the entire string is enclosed in quotes.")

# Add optional flag to convert to Cisco format
parser.add_argument("-c", "--cisco",
                    help="Converts MAC address to Cisco \
                    xxxx.xxxx.xxxx format. This is the default if no format is \
                    chosen.",
                    action='store_true')

# Add optional flag to convert to colon format
parser.add_argument("-n","--colon",
                    help="Converts MAC address to colon \
                    separated xx:xx:xx:xx:xx:xx format.",
                    action='store_true')

# Add optional flag to convert to dash format
parser.add_argument("-d","--dash",
                    help="Converts MAC address to dash separated \
                    xx-xx-xx-xx-xx-xx format.",
                    action='store_true')

# Add optional flag to convert to empty format
parser.add_argument("-e","--empty",
                    help="Converts MAC address to empty xxxxxxxxxxxx format.",
                    action='store_true')

# Add optional flag to skip printing MAC address OUI.
parser.add_argument("-s","--skip",
                    help="Skips printing of MAC address OUI.",
                    action='store_true')

# Add optional flag reads list of MACs from file and print OUI for each
parser.add_argument("-f","--file",
                    help="Provide a list of MAC addresses from a file and \
                    print the OUI for each.")

# Add optional flag to display program version information
parser.add_argument("-v","--version",
                    help="Prints program version information.",
                    action='store_true')

# Create group for options that are mutually exclusive
group = parser.add_mutually_exclusive_group()

# Add optional flag to convert to MAC to upper case, mutually exclusive to -l
group.add_argument("-u","--upper",
                    help="Converts MAC address output to uppercase.",
                    action='store_true')
# Add optional flag to convert to MAC to lower case, mutually exclusive to -u
group.add_argument("-l","--lower",
                    help="Converts MAC address output to lowercase.",
                    action='store_true')

# Parse CLI arguments
arguments = parser.parse_args()

# -f --file FILE exists
if (arguments.file):
    macs = fileLookup(arguments.file)
    if macs[0] != "false":
        for i in range(len(macs)):
            if "ERROR" not in macs[i]:
                if arguments.lower:
                    print(macs[i].lower() + ": " + ouiLookup(macs[i]))
                if arguments.upper:
                    print(macs[i].upper() + ": " + ouiLookup(macs[i]))
                else:
                    print(macs[i] + ": " + ouiLookup(macs[i]))
            else:
                print("ERROR: MAC address invalid.")

# Check for valid MAC address
if(arguments.MAC and validMAC(arguments.MAC)):
    # Format MAC argument 
    arguments.MAC = validMAC(arguments.MAC)
    # Convert MAC arguments, if any
    formats = convertMAC(arguments)

    # -u --upper arguments exists
    if formats and arguments.upper:
        for key,value in formats.items():
            formats[key] = value.upper()

    # -l --lower arguments exists
    if formats and arguments.lower:
        for key,value in formats.items():
            formats[key] = value.lower()

    # Print converted arguments and OUI, if any
    if formats:
        if "colon" in formats:
            print(formats["colon"])
        if "dash" in formats:
            print(formats["dash"])
        if "empty" in formats:
            print(formats["empty"])
        if "cisco" in formats:
            print(formats["cisco"])
        if "oui" in formats:
                print(formats["oui"])

# -v --version exists
if arguments.version:
    print('Version 3.1 - 11/07/2016')
